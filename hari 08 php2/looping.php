
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Looping</title>
</head>
<body>
    <h1>Berlatih Looping</h1>

    <?php 
        echo "<h3>Soal No 1 Looping I Love PHP</h3>"; // SOAL NO 1

        echo "Looping Pertama <br><br>";
        for($nilai=2; $nilai<=20; $nilai+=2){
            echo $nilai . " - I Love PHP<br>";
        }
        echo "Looping Kedua <br><br>";
        for($nilai=20; $nilai>=2; $nilai-=2){
            echo $nilai . " - I Love PHP<br>";
        }




        echo "<h3>Soal No 2 Looping Array Modulo </h3>"; // SOAL NO 2

        $numbers = [18, 45, 29, 61, 47, 34];
        echo "array numbers: ";
        print_r($numbers);
        foreach($numbers as $value)
        {
            $rest[] = $value %=5;
        }

        echo "<br>";
        echo "Array sisa baginya adalah:  "; 
        print_r($rest);
        echo "<br>";

        echo "<h3> Soal No 3 Looping Asociative Array </h3>"; // SOAL NO 3

        $items = [
            ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpeg'], 
            ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpeg'],
            ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
            ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpeg']
        ];
        
        // Output: 
        foreach($items as $value){
            $tampung = [
                'id' => $value[0],
                'name' => $value[1],
                'price' => $value[2],
                'description' => $value[3],
                'source' => $value[4],
            ];

            print_r ($tampung);
            echo "<br>";


        }
        
        echo "<h3>Soal No 4 Asterix </h3>"; // SOAL NO 4

        echo "Looping Pertama <br><br>";
        echo "Asterix : 5x5 <br><br>";
        for($bintang=10; $bintang>=1; $bintang--){
            for($i=$bintang; $i<=10; $i++){
                echo "* ";
            }       
            echo "<br>";         
        }

    ?>

</body>
</html>