<?php

require_once 'animal.php';
require_once 'frog.php';
require_once 'ape.php';

$animal = new hewan("Domba");

echo "Jenis Hewan : " . $animal->name . "<br>";
echo "Jumlah Kaki : " . $animal->kaki . "<br>";
echo "Darah Dingin : " . $animal->darah_dingin . "<br><br>";

$hewanbaru1 = new frog("Kodok");

echo "Jenis Hewan : " . $hewanbaru1->name . "<br>";
echo "Jumlah Kaki : " . $hewanbaru1->kaki . "<br>";
echo "Darah Dingin : " . $hewanbaru1->darah_dingin . "<br>";
echo $hewanbaru1->jump("Hip Hop<br><br>");

$hewanbaru2 = new ape("Monyet");

echo "Jenis Hewan : " . $hewanbaru2->name . "<br>";
echo "Jumlah Kaki : " . $hewanbaru2->kaki . "<br>";
echo "Darah Dingin : " . $hewanbaru2->darah_dingin . "<br>";
echo $hewanbaru2->suara("Auooooo")



?>