<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>
<body>
    <h1>Berlatih String PHP</h1>
    <?php   
        echo "<h3> Soal No 1</h3>"; // soal no 1

        $first_sentence = "Hello PHP!" ; 
        echo "kalimat 1 = " . $first_sentence . "<br>";
        echo "Panjang string = " . strlen($first_sentence) ."<br>";
        echo "Jumlah kata = " . str_word_count($first_sentence) ."<br><br>";        

        $second_sentence = "I'm ready for the challenges"; 
        echo "kalimat 2 = " . $second_sentence . "<br>";
        echo "Panjang string = " . strlen($second_sentence) ."<br>";
        echo "Jumlah kata = " . str_word_count($second_sentence) ."<br><br>";
        
        echo "<h3> Soal No 2</h3>"; // soal no 2

        $string2 = "I love PHP";        
        echo "<label>String: </label> \"$string2\" <br>";
        echo "Kata pertama : " . substr($string2, 0, 1) . "<br>" ; 
        echo "Kata kedua : " . substr($string2, 2, 5) . "<br>" ;
        echo "Kata Ketiga : " . substr($string2, 6, 9) . "<br>" ;

        echo "<h3> Soal No 3 </h3>"; // soal no 3

        $string3 = "PHP is old but sexy!";
        echo "String : \"$string3\" <br>"; 
        echo "mengganti string : " . str_replace("sexy","awesome",$string3);

    ?>
</body>
</html>