/************PRELOADER*************/
$(window).on("load", function () {
    $(".preload").fadeOut("slow").addClass("loaded");
});
/**********END_PRELOADER***********/

try {
    if (topbar) {
        topbar.config({
            autoRun: true,
            barThickness: 3,
            barColors: {
                '0': 'rgba(78,180,202,255)',
                '.25': 'rgba(15,168,196,255)',
                '.50': 'rgba(55,110,175,255)',
                '.75': 'rgba(64,98,152,255)',
                '1.0': 'rgba(21,56,119,255)'
            },
            shadowBlur: 10,
            shadowColor: 'rgba(0,   0,   0,   .6)'
        })
    }
} catch (error) {
    
}
